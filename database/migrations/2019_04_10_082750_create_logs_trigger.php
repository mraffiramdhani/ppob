<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('CREATE TRIGGER log_update_user AFTER UPDATE ON `users` FOR EACH ROW
                BEGIN
                   INSERT INTO `user_log` (`id_pelanggan`, `username`, `password`, `nomor_kwh`, `nama_pelanggan`, `alamat`, `id_tarif`, `log_status`) VALUES (NEW.id_pelanggan, NEW.username, NEW.password, NEW.nomor_kwh, NEW.nama_pelanggan, NEW.alamat, NEW.id_tarif, "Updated");
                END');

        DB::unprepared('CREATE TRIGGER log_create_penggunaan AFTER INSERT ON `penggunaans` FOR EACH ROW
                BEGIN
                   INSERT INTO `penggunaan_log` (`id_penggunaan`, `id_pelanggan`, `bulan`, `tahun`, `meter_awal`, `meter_akhir`, `log_status`) VALUES (NEW.id_penggunaan, NEW.id_penggunaan, NEW.bulan, NEW.tahun, NEW.meter_awal, NEW.meter_akhir, "Created");
                END');

        DB::unprepared('CREATE TRIGGER log_create_tagihan AFTER INSERT ON `tagihans` FOR EACH ROW
                BEGIN
                   INSERT INTO `tagihan_log` (`id_tagihan`, `id_penggunaan`, `id_pelanggan`, `bulan`, `tahun`, `jumlah_meter`, `id_status`, `log_status`) VALUES (NEW.id_tagihan, NEW.id_penggunaan, NEW.id_pelanggan, NEW.bulan, NEW.tahun, NEW.jumlah_meter, NEW.id_status, "Created");
                END');

        DB::unprepared('CREATE TRIGGER log_create_pembayaran AFTER INSERT ON `pembayarans` FOR EACH ROW
                BEGIN
                   INSERT INTO `pembayaran_log` (`id_tagihan`, `id_pembayaran`, `id_pelanggan`, `tanggal_pembayaran`, `bulan_bayar`, `biaya_admin`, `total_bayar`, `akun_bank`, `id_admin`, `log_status`) VALUES (NEW.id_tagihan, NEW.id_pembayaran, NEW.id_pelanggan, NEW.tanggal_pembayaran, NEW.bulan_bayar, NEW.biaya_admin, NEW.total_bayar, NEW.akun_bank, NEW.id_admin, "Created");
                END');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER IF EXISTS `log_update_user`');
        DB::unprepared('DROP TRIGGER IF EXISTS `log_create_penggunaan`');
        DB::unprepared('DROP TRIGGER IF EXISTS `log_create_tagihan`');
        DB::unprepared('DROP TRIGGER IF EXISTS `log_create_pembayaran`');
    }
}
