<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPenggunaanTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('CREATE TRIGGER add_penggunaan_trigger AFTER INSERT ON `users` FOR EACH ROW
                BEGIN
                   INSERT INTO `penggunaans` (`id_pelanggan`, `bulan`, `tahun`, `meter_awal`, `meter_akhir`, `created_at`, `updated_at`) VALUES (NEW.id_pelanggan, CURRENT_DATE(), CURRENT_DATE(), 0.0, 0.0, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());
                   INSERT INTO `user_log` (`id_pelanggan`, `username`, `password`, `nomor_kwh`, `nama_pelanggan`, `alamat`, `id_tarif`, `log_status`) VALUES (NEW.id_pelanggan, NEW.username, NEW.password, NEW.nomor_kwh, NEW.nama_pelanggan, NEW.alamat, NEW.id_tarif, "Created");
                END');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER IF EXISTS `add_penggunaan_trigger`');
    }
}
