<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePembayaransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembayarans', function (Blueprint $table) {
            $table->increments('id_pembayaran');
            $table->integer('id_tagihan')->unsigned();
            $table->integer('id_pelanggan')->unsigned();
            $table->timestamp('tanggal_pembayaran');
            $table->date('bulan_bayar');
            $table->decimal('biaya_admin',14,2)->default(2000);
            $table->decimal('total_bayar',14,2);
            $table->string('akun_bank')->nullable();
            $table->integer('id_admin')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('id_tagihan')->references('id_tagihan')->on('tagihans')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_pelanggan')->references('id_pelanggan')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_admin')->references('id_admin')->on('admins')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembayarans');
    }
}
