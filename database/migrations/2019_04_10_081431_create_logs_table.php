<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_pelanggan');
            $table->string('username');
            $table->string('password');
            $table->string('nomor_kwh');
            $table->string('nama_pelanggan');
            $table->text('alamat');
            $table->integer('id_tarif');
            $table->string('log_status');
            $table->timestamps();
        });
        Schema::create('penggunaan_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_penggunaan');
            $table->integer('id_penggunaan');
            $table->date('bulan');
            $table->date('tahun');
            $table->double('meter_awal');
            $table->double('meter_akhir');
            $table->string('log_status');
            $table->timestamps();
        });
        Schema::create('tagihan_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_tagihan');
            $table->integer('id_penggunaan');
            $table->integer('id_pelanggan');
            $table->date('bulan');
            $table->date('tahun');
            $table->double('jumlah_meter');
            $table->integer('id_status');
            $table->string('log_status');
            $table->timestamps();
        });
        Schema::create('pembayaran_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_pembayaran');
            $table->integer('id_tagihan');
            $table->integer('id_pelanggan');
            $table->timestamp('tanggal_pembayaran');
            $table->date('bulan_bayar');
            $table->decimal('biaya_admin',14,2);
            $table->decimal('total_bayar',14,2);
            $table->string('akun_bank');
            $table->integer('id_admin');
            $table->string('log_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_log');
        Schema::dropIfExists('penggunaan_log');
        Schema::dropIfExists('tagihan_log');
        Schema::dropIfExists('pembayaran_log');
    }
}
