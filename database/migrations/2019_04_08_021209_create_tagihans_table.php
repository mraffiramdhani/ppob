<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagihansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tagihans', function (Blueprint $table) {
            $table->increments('id_tagihan');
            $table->integer('id_penggunaan')->unsigned();
            $table->integer('id_pelanggan')->unsigned();
            $table->date('bulan');
            $table->date('tahun');
            $table->double('jumlah_meter');
            $table->integer('id_status')->unsigned();
            $table->timestamps();

            $table->foreign('id_penggunaan')->references('id_penggunaan')->on('penggunaans')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_pelanggan')->references('id_pelanggan')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_status')->references('id_status')->on('status')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tagihans');
    }
}
