<?php

use Illuminate\Database\Seeder;

class LevelSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $level = [
        	['id_level' => 1, 'nama_level' => 'Administrator'],
        	['id_level' => 2, 'nama_level' => 'Bank'],
        ];

        foreach($level as $data){
        	\App\Level::create($data);
        }
    }
}
