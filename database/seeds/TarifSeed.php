<?php

use Illuminate\Database\Seeder;

class TarifSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tarif = [
        	['id_tarif' => 1, 'daya' => 'R-1/450 VA', 'tarifperkwh' => 415],
        	['id_tarif' => 2, 'daya' => 'R-1/900 VA', 'tarifperkwh' => 568],
            ['id_tarif' => 3, 'daya' => 'R-1/900 VA RTM', 'tarifperkwh' => 1352],
            ['id_tarif' => 4, 'daya' => 'R-1/1.300 VA', 'tarifperkwh' => 1467.28],
        	['id_tarif' => 5, 'daya' => 'R-1/2.200 VA', 'tarifperkwh' => 1467.28],
            ['id_tarif' => 6, 'daya' => 'R-1/3.500 VA', 'tarifperkwh' => 1467.28],
        ];

        foreach($tarif as $data){
        	\App\Tarif::create($data);
        }
    }
}
