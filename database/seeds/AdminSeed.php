<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = [
        	['id_admin' => 1, 'nama_admin' => 'Administrator', 'username' => 'admin', 'password' => Hash::make('admin123'), 'id_level' => 1],
        	['id_admin' => 2, 'nama_admin' => 'Bank', 'username' => 'bank', 'password' => Hash::make('bank123'), 'id_level' => 2],
        ];

        foreach($admin as $data){
        	\App\Admin::create($data);
        }
    }
}
