<?php

use Illuminate\Database\Seeder;

class StatusSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = [
        	['id_status' => 1, 'deskripsi' => 'Belum Dilunasi'],
        	['id_status' => 2, 'deskripsi' => 'Menunggu Konfirmasi'],
        	['id_status' => 3, 'deskripsi' => 'Lunas'],
        ];

        foreach($status as $data){
        	\App\Status::create($data);
        }
    }
}
