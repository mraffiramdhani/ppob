<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$this->call(LevelSeed::class);
    	$this->call(AdminSeed::class);
        $this->call(TarifSeed::class);
        $this->call(StatusSeed::class);
    }
}
