<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Balistrik</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="{{asset('/css/bootstrap.min.css')}}">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
            .bg-pic{
            	background-image: url('images/bg-pic.jpg');
            	background-size: cover;
            	background-position: center;
            	background-repeat: no-repeat;
            	background-color: #bfbfbf;
            	background-blend-mode: screen;
            }
            .white{
            	color: white;
            }
        </style>
    </head>
    <body>

			<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
			  <a class="navbar-brand" href="#"><img src="{{ asset('images/electricity.png') }}" width="50px">Balistrik</a>
			  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
			    <span class="navbar-toggler-icon"></span>
			  </button>
			  <div class="collapse navbar-collapse" id="collapsibleNavbar">
			    <ul class="navbar-nav">
			    @if(Route::has('login'))
			    	@if(Auth::guest())
				      <li class="nav-item">
				        <a class="nav-link" href="{{ url('/login') }}">Login</a>
				      </li>
				      <li class="nav-item">
				        <a class="nav-link" href="{{ url('/register') }}">Register</a>
				      </li>
				    @else
				      <li class="nav-item">
				        <a class="nav-link" href="{{ url('/home') }}">Home</a>
				      </li>
				    @endif
			    @endif   
			    </ul>
			  </div>  
			</nav>

    	<div class="jumbotron text-center bg-pic" style="margin-bottom:0">
		  <img src="{{ asset('images/electricity.png') }}" width="100px"><h1>Balistrik</h1>
		  <p>Kemudahan membayar tagihan listrik pascabayar anda.</p> 
		</div>

		<div class="jumbotron text-center bg-dark white" style="margin-bottom:0">
		  <h1>Coba Sekarang</h1>
		  <p>Bergabung dengan jutaan pengguna lainnya untuk menikmati kemudahan membayar tagihan listrik anda</p>
		  <a href="{{ url('/register') }}" class="btn btn-outline-primary btn-lg">Register</a> Atau
		  <a href="{{ url('/login') }}" class="btn btn-secondary btn-lg">Login</a>
		</div>

		{{-- <div class="container" style="margin-top:30px">
		  <div class="row">
		    <div class="col-sm-4">
		      <h2>About Me</h2>
		      <h5>Photo of me:</h5>
		      <div class="fakeimg">Fake Image</div>
		      <p>Some text about me in culpa qui officia deserunt mollit anim..</p>
		      <h3>Some Links</h3>
		      <p>Lorem ipsum dolor sit ame.</p>
		      <ul class="nav nav-pills flex-column">
		        <li class="nav-item">
		          <a class="nav-link active" href="#">Active</a>
		        </li>
		        <li class="nav-item">
		          <a class="nav-link" href="#">Link</a>
		        </li>
		        <li class="nav-item">
		          <a class="nav-link" href="#">Link</a>
		        </li>
		        <li class="nav-item">
		          <a class="nav-link disabled" href="#">Disabled</a>
		        </li>
		      </ul>
		      <hr class="d-sm-none">
		    </div>
		    <div class="col-sm-8">
		      <h2>TITLE HEADING</h2>
		      <h5>Title description, Dec 7, 2017</h5>
		      <div class="fakeimg">Fake Image</div>
		      <p>Some text..</p>
		      <p>Sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
		      <br>
		      <h2>TITLE HEADING</h2>
		      <h5>Title description, Sep 2, 2017</h5>
		      <div class="fakeimg">Fake Image</div>
		      <p>Some text..</p>
		      <p>Sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.</p>
		    </div>
		  </div>
		</div> --}}

		<div class="jumbotron text-center" style="margin-bottom:0">
		  <p>@Balistrik 2019</p>
		</div>
    <script type="text/javascript" src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    </body>
</html>
