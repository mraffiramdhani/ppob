@extends('layouts.app')

<?php
    $jumlah = $penggunaan->meter_akhir - $penggunaan->meter_awal;
?>

@section('content')
@if (session('msg'))
    <div class="alert alert-success text-center">
        <strong>Success:</strong> {{ session('msg') }}
    </div>
@endif

<div class="container">

    <div class="row mt-lg-4">
      <div class="col-sm-4">
        <div class="card" style="width: 18rem;">
          <div class="card-body">
            <h5 class="card-title">Informasi Pelanggan</h5>
          </div>
          <ul class="list-group list-group-flush">
            <li class="list-group-item">Nama :<div style="float: right;">{{ Auth::user()->nama_pelanggan }}</div></li>
            <li class="list-group-item">ID Pelanggan :<div style="float: right;">{{ Auth::user()->nomor_kwh }}</div></li>
            <li class="list-group-item">Alamat :<div style="float: right;">{{ Auth::user()->alamat }}</div></li>
            <li class="list-group-item">Gol/Tarif :<div style="float: right;">{{ Auth::user()->tarif->daya }}</div></li>
          </ul>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="card">
            <div class="card">
              <h5 class="card-header">Penggunaan Terbaru</h5>
              <div class="card-body">
                <div style="float: left;">
                    <h5 class="card-title">Bulan : {{ date('F', strtotime($penggunaan->bulan)) }}</h5>
                    <p class="card-text">Penggunaan : {{ $jumlah }} KWh</p>
                </div>
                {{-- <div style="float: right;">
                  @if($tagihan->status->id_status == 1)
                    <span style="color: red;">{{ $tagihan->status->deskripsi }}</span><br/>
                      <a href="{{ route('user.payment', ['id' => $tagihan->id_tagihan]) }}" onclick="event.preventDefault(); document.getElementById('payment_form').submit();" class="btn btn-success btn-lg">Bayar</a>

                      <form id="payment_form" action="{{ route('user.payment') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                        <input name="id" value="{{ $tagihan->id_tagihan }}" hidden>
                      </form>
                  @else
                      <span style="color: green;">{{ $tagihan->status->deskripsi }}</span><br/>
                  @endif
                </div> --}}
              </div>
            </div>
        </div>
      </div>
    </div>

    <div class="row mt-lg-2 mb-lg-3">
      <div class="col-sm-12">
        <div class="card text-center">
          <div class="card-header">
            Daftar Tagihan
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-hover text-center">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Bulan</th>
                    <th scope="col">Tahun</th>
                    <th scope="col">Jumlah Meter</th>
                    <th scope="col">Total</th>
                    <th scope="col">Status</th>
                    <th scope="col">Opsi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1 ?>
                  @if($tg_all->count() == 0)
                      <tr>
                          <td colspan="6">Tidak Ada Data</td>
                      </tr>
                  @else
                      @foreach($tg_all as $data)
                          <tr>
                              <td>{{ $no }}</td>
                              <td>{{ date('F', strtotime($data->bulan)) }}</td>
                              <td>{{ date('Y', strtotime($data->tahun)) }}</td>
                              <td>{{ $data->jumlah_meter }}</td>
                              <td>Rp.{{ $data->jumlah_meter * $data->user->tarif->tarifperkwh }},-</td>
                              <td>{{ $data->status->deskripsi }}</td>
                              <td>
                                  @if($data->status->id_status == 1 && $data->pembayaran == null)
                                      <a href="#" class="btn btn-success btn-sm payment-btn" data-id="{{ $data->id_tagihan }}" data-bulan="{{ date('F Y', strtotime($data->bulan)) }}" data-total="Rp.{{ $data->jumlah_meter * $data->user->tarif->tarifperkwh + 2000 }},-">Bayar</a>
                                  @else
                                      <a href="{{route('user.bill.print', ['id' => $data->id_tagihan])}}" class="btn btn-success btn-sm" id="print_bill">Lihat Struk</a>
                                  @endif
                              </td>
                          </tr>
                          <?php $no++; ?>
                      @endforeach
                  @endif
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content text-center">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Pembayaran</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h1>Pembayaran Tagihan</h1>
        <h4>Atas Nama</h4>
        <p>{{ Auth::user()->nama_pelanggan }}</p> 
        <h4>Bulan Penggunaan</h4>
        <p id="bulan-penggunaan">N/A</> 
        <h4>Total Bayar </h4><small>+ Biaya Admin</small>
        <p id="total-bayar" style="color: green;font-size: 20px;">N/A</p> 
        <small>Silakan masukkan nomor akun bank anda</small>
        <form id="payment-form" action="" method="POST">
          {{ csrf_field() }}
          <input type="number" maxlength="16" minlength="16" name="akun_bank" class="form-control text-center">
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="event.preventDefault(); document.getElementById('payment-form').submit();">Confirm</button>
      </div>
    </div>
  </div>
</div>
<!-- end of modal -->

@endsection

@section('script')

<script type="text/javascript">
  $('.payment-btn').on('click', function(e){
    e.preventDefault();
    $('#paymentModal').modal('show');
    $url = window.location.origin;
    if($url == 'http://localhost:8120'){
      $('#payment-form').attr('action', window.location.origin + '/ppob/public/user/payment/' + $(this).data('id'));
    }else{
      $('#payment-form').attr('action', window.location.origin + '/user/payment/' + $(this).data('id'));
    }
    $('#bulan-penggunaan').html($(this).data('bulan'));
    $('#total-bayar').html($(this).data('total'));
  });
</script>

@endsection