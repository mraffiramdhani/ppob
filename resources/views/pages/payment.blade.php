@extends('layouts.app')

<?php
    $date = date_create($tagihan->bulan);
    $result = date_format($date, 'F/Y');

    $total = $tagihan->jumlah_meter * Auth::user()->tarif->tarifperkwh;
?>

@section('content')
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">Pembayaran Tagihan</div>

            <div class="panel-body text-center">
                <span><h3>Tagihan Listrik</h3></span>
                <span><h4>Bulan : {{ $result }}</h4></span>
                <span><h4>Atas Nama<br/>{{ Auth::user()->nama_pelanggan }}</h4><br/></span>
                <span><h4>Total Bayar</h4><h2 style="color: green;">Rp.{{ $total }},-</h2></span>
                <span><small>Silakan masukkan nomor akun bank anda</small></span>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <form action="{{ route('user.payment.submit') }}" method="POST">
                            {{ csrf_field() }}
                            <input type="number" maxlength="16" minlength="16" name="akun_bank" class="form-control text-center">
                            <input type="text" name="total_bayar" value="{{$total}}" hidden>
                            <input type="text" name="id_tagihan" value="{{$tagihan->id_tagihan}}" hidden>
                            <input type="text" name="bulan" value="{{$tagihan->bulan}}" hidden>
                            <br/>
                            <div class="col-md-6 col-md-offset-3">
                                <a href="{{ route('home') }}" class="btn btn-danger btn-md">Batal</a>
                                <input type="submit" name="submit" class="btn btn-primary btn-md" value="Konfirmasi">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

    <script type="text/javascript"></script>

@endsection
