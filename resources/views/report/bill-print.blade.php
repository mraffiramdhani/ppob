<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Balistrik - Struk Pembayaran</title>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:100,600" rel="stylesheet" type="text/css">
    <style type="text/css">
            .header{
                width: 100%;
                height: 100px;
                background-color: lightgrey;
            }
            .title{
                color: #fff;
                font-size: 70px;
                float: left;
            }
            .sub-title{
                color: #fff;
                font-size: 30px;
                float: right;
            }
            .body{
                width: 100%;
                border: 1px solid black;
                height: 280px;
                padding: 14px 16px;
            }
    </style>
</head>
<body>
    <div class="header">
        <p class="title">Balistrik</p>
        <p class="sub-title">{{ date('d F Y', strtotime(now())) }}</p>
    </div>

    <div class="body">
        <p>&nbsp;</p>
        <p>IDPEL : {{$tagihan->user->nomor_kwh}}</p>
        <p>NAMA : {{$tagihan->user->nama_pelanggan}}</p>
        <p>BULAN : {{date('M/Y', strtotime($tagihan->bulan))}}</p>
        <p>ADMIN : Rp.{{$tagihan->pembayaran->biaya_admin}}</p>
        <p>TOTAL : Rp.{{$tagihan->pembayaran->total_bayar}}
        <div style="float: right;">{{$tagihan->status->deskripsi}}</div></p>
    </div>
</body>
</html>
