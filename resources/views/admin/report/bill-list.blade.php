<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Balistrik - Laporan Daftar Tagihan</title>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:100,600" rel="stylesheet" type="text/css">
    <style type="text/css">
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
            table{
                width: 100%;
                margin: 14px 16px;
                margin-top: 60px;
            }
            th {
                border: 2px solid black;
                background-color: lightgrey;
            }
            td {
                border: 2px solid black;
            }
    </style>
</head>
<body>
  <div class="text-center" style="margin-top: 30px;">
    <h1>Balistrik</h1>
    <h3>Laporan Data Tagihan</h3>

    <div>
        <table>
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama Pelanggan</th>
                    <th>ID Pelanggan</th>
                    <th>Bulan</th>
                    <th>Jumlah Meter</th>
                    <th>Total</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($bill as $data)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $data->user->nama_pelanggan }}</td>
                    <td>{{ $data->user->nomor_kwh }}</td>
                    <td>{{ date('F/Y', strtotime($data->bulan)) }}</td>
                    <td>{{ $data->jumlah_meter }} KWh</td>
                    <td>Rp.{{ round($data->pembayaran->total_bayar) }},-</td>
                    <td>{{ $data->status->deskripsi }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
  </div>
</body>
</html>
