<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Balistrik - Laporan Daftar Pelanggan</title>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:100,600" rel="stylesheet" type="text/css">
    <style type="text/css">
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
            table{
                width: 100%;
                margin: 14px 16px;
                margin-top: 60px;
            }
            th {
                border: 2px solid black;
                background-color: lightgrey;
            }
            td {
                border: 2px solid black;
            }
    </style>
</head>
<body>
  <div class="text-center" style="margin-top: 30px;">
    <h1>Balistrik</h1>
    <h3>Laporan Data Pelanggan</h3>

    <div>
        <table>
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama Pelanggan</th>
                    <th>ID Pelanggan</th>
                    <th>Alamat</th>
                    <th>Gol/Tarif</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $data)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $data->nama_pelanggan }}</td>
                    <td>{{ $data->nomor_kwh }}</td>
                    <td>{{ $data->alamat }}</td>
                    <td>{{ $data->tarif->daya }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
  </div>
</body>
</html>
