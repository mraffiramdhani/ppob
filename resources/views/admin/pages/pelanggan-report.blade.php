<!DOCTYPE html>
<html>
<head>
    <title>Laporan Daftar Pelanggan</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>

    <div id="header" class="text-center">
        <h1>Balistrik</h1>
        <h2>Laporan Daftar Pelanggan</h2>
    </div>

<script type="text/javascript" src="{{ asset('/js/app.js') }}"></script>
</body>
</html>