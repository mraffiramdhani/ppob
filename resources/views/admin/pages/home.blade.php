@extends('admin.layouts.app')

@section('content')

@if (session('msg'))
    <div class="alert alert-success text-center">
        <strong>Success:</strong> {{ session('msg') }}
    </div>
@endif
<div class="container">
@if(Auth::user()->id_level == 1)
    <nav class="nav nav-pills nav-justified mt-lg-2 mb-lg-2">
      <a class="nav-link active" href="#">Home</a>
      <a class="nav-link" href="{{ route('admin.register.new') }}">Tambah Admin</a>
    </nav>
@else
@endif

  @if(Auth::user()->id_level == 2)

    <div class="row mt-lg-4 mb-lg-4">
      <div class="col-sm-12">
        <div class="card">
            <div class="card">
              <h5 class="card-header">Data Pembayaran</h5>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-hover text-center">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Tanggal Pembayaran</th>
                        <th scope="col">Bulan Bayar</th>
                        <th scope="col">Total Bayar</th>
                        <th scope="col">Akun Bank</th>
                        <th scope="col">Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $no = 1; ?>
                        @foreach($pembayaran as $data)
                          @if($data->id_admin == null)
                              <tr>
                                <td>{{$no}}</td>
                                <td>{{ $data->user->nama_pelanggan }}</td>
                                <td>{{ date('d F- Y', strtotime($data->tanggal_pembayaran)) }}</td>
                                <td>{{ date('F', strtotime($data->bulan_bayar)) }}</td>
                                <td>{{ $data->total_bayar }}</td>
                                <td>{{ $data->akun_bank }}</td>
                                <td><a href="{{ route('admin.payment.confirm', ['id' => $data->id_pembayaran]) }}" class="btn btn-success btn-sm">Konfirmasi</a></td>
                              </tr>
                          @else
                              <tr>
                                <td>{{$no}}</td>
                                <td>{{ $data->user->nama_pelanggan }}</td>
                                <td>{{ date('d F- Y', strtotime($data->tanggal_pembayaran)) }}</td>
                                <td>{{ date('F', strtotime($data->bulan_bayar)) }}</td>
                                <td>{{ $data->total_bayar }}</td>
                                <td>{{ $data->akun_bank }}</td>
                                <td>None</td>
                              </tr>
                          @endif
                          <?php $no++; ?>
                        @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>

  @else

    <div class="row mt-lg-4">
      <div class="col-sm-12">
        <div class="card">
            <div class="card">
              <h5 class="card-header">Data Pelanggan<a href="{{route('admin.report.user-list')}}" class="btn btn-primary btn-sm" style="float: right;">Print Laporan</a></h5>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-hover text-center">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama</th>
                        <th scope="col">ID Pelanggan</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Gol/Tarif</th>
                        <th scope="col">Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $no = 1; ?>
                        @foreach($users as $data)
                          <tr>
                            <td>{{$no}}</td>
                              <td>{{ $data->nama_pelanggan }}</td>
                              <td>{{ $data->nomor_kwh }}</td>
                              <td>{{ $data->alamat }}</td>
                              <td>{{ $data->tarif->daya }}</td>
                              <td><a href="{{ route('admin.user.edit', ['id' => $data->id_pelanggan]) }}" class="btn btn-primary btn-sm">Edit</a> &middot; 
                                <a href="#" class="btn btn-danger btn-sm delete-data" data-id="{{$data->id_pelanggan}}" >Delete</a></td>
                            </tr>
                          <?php $no++; ?>
                        @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
      <div class="col-sm-12 mt-lg-2 mb-lg-4">
        <div class="card">
            <div class="card">
              <h5 class="card-header">Data Penggunaan<a href="{{route('admin.report.bill-list')}}" class="btn btn-primary btn-sm" style="float: right;">Print Laporan</a></h5>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-hover text-center">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Bulan</th>
                        <th scope="col">Tahun</th>
                        <th scope="col">Meter Awal</th>
                        <th scope="col">Meter Akhir</th>
                        <th scope="col">Opsi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $no = 1; ?>
                        @foreach($penggunaan as $data)
                          <tr>
                            <td>{{$no}}</td>
                              <td>{{ $data->user->nama_pelanggan }}</td>
                              <td>{{ date('F', strtotime($data->bulan)) }}</td>
                              <td>{{ date('Y', strtotime($data->tahun)) }}</td>
                              <td>{{ $data->meter_awal }}</td>
                              <td>{{ $data->meter_akhir }}</td>
                              @if($data->tagihan == null)
                                <td><a href="{{ route('admin.bill.send', ['id' => $data->id_penggunaan]) }}" class="btn btn-success btn-sm">Kirim Tagihan</a></td>
                              @elseif($data->tagihan->id_status == 2 && $data->tagihan->pembayaran->id_admin != null)
                                <td><a href="{{ route('admin.bill.confirm', ['id' => $data->tagihan->id_tagihan]) }}" class="btn btn-primary btn-sm">Konfirmasi</a></td>
                              @elseif($data->tagihan->id_status == 2)
                                <td>Menunggu Konfirmasi Bank</td>
                              @else
                                <td>None</td>
                              @endif
                            </tr>
                          <?php $no++; ?>
                        @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  @endif
</div>

<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Hapus Data Pengguna</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah anda yakin ingin melanjutkan?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="event.preventDefault(); document.getElementById('delete-form').submit();">Confirm</button>
        <form id="delete-form" action="" method="POST" style="display: none;">
          {{ csrf_field() }}
        </form>
      </div>
    </div>
  </div>
</div>
<!-- end of modal -->

@endsection

@section('script')

<script type="text/javascript">
  $('.delete-data').on('click', function(e){
    e.preventDefault();
    $('#deleteModal').modal('show')
    $('#delete-form').attr('action', 'admin/u/delete/' + $(this).data('id'));
  });
</script>

@endsection
