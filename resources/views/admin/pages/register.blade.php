@extends('admin.layouts.app')

@section('content')

@if (session('msg'))
    <div class="alert alert-success text-center">
        <strong>Success:</strong> {{ session('msg') }}
    </div>
@endif

@if(Auth::user()->id_level == 1)
    <nav class="nav nav-pills nav-justified mt-lg-2 mb-lg-2">
      <a class="nav-link" href="{{ route('admin.dashboard') }}">Home</a>
      <a class="nav-link active" href="#">Tambah Admin</a>
    </nav>
@else
@endif
<div class="card">
    <article class="card-body">
        <h4 class="card-title mb-4 mt-1">Register</h4>
        <form class="form-horizontal" method="POST" action="{{ route('admin.register.submit') }}">
            {{ csrf_field() }}
            <div class="form-group">
                <label>Fullname</label>
                <input name="name" class="form-control" placeholder="Fullname" type="text">
                @if ($errors->has('name'))
                    <span class="help-block alert-danger">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div> <!-- form-group// -->
            <div class="form-group">
                <label>Username</label>
                <input name="username" class="form-control" placeholder="Username" type="username">
                @if ($errors->has('username'))
                    <span class="help-block alert-danger">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
            </div> <!-- form-group// -->
            <div class="form-group">
                <label>Password</label>
                <input class="form-control" placeholder="******" type="password" name="password">
                @if ($errors->has('password'))
                    <span class="help-block alert-danger">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div> <!-- form-group// --> 
            <div class="form-group">
                <label>Confirm password</label>
                <input class="form-control" placeholder="******" type="password" name="password_confirmation">
            </div> <!-- form-group// --> 
            <div class="form-group">
                <label>Level</label>
                <select name="id_level" class="form-control">
                    <option value="2">Administrator</option>
                    <option value="3">Bank</option>
                </select>
            </div> <!-- form-group// --> 
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block"> Register  </button>
            </div> <!-- form-group// -->                                                           
        </form>
    </article>
</div> <!-- card.// -->

{{-- <div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">

            <div class="panel-body">
                <a href="{{ route('admin.dashboard') }}" class="nav navbar-nav">Home</a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">Daftar Admin Baru</a></div>

            <div class="panel-body">
                <!-- <div class="container"> -->
                    <form class="form-horizontal" method="POST" action="{{ route('admin.register.submit') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-4 control-label">Username</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required>

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="id_level" class="col-md-4 control-label">Level</label>

                            <div class="col-md-6">
                                <select name="id_level" class="form-control">
                                    <option value="2">Administrator</option>
                                    <option value="3">Bank</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                <!-- </div> -->
            </div>
        </div>
    </div>
</div> --}}
@endsection
