@extends('admin.layouts.app')

@section('content')

@if(Auth::user()->id_level == 1)
    <nav class="nav nav-pills nav-justified mt-lg-2 mb-lg-2">
      <a class="nav-link" href="{{ route('admin.dashboard') }}">Home</a>
      <a class="nav-link" href="#">Tambah Admin</a>
    </nav>
@else
@endif
<div class="card">
    <article class="card-body">
        <h4 class="card-title mb-4 mt-1">Update User Data</h4>
        <form class="form-horizontal" method="post" action="{{ route('admin.user.edit.submit', ['id' => $user->id_pelanggan]) }}">
            {{ csrf_field() }}
            {{ method_field("PATCH") }}
            <div class="form-group">
                <label>Nama Lengkap</label>
                <input name="name" class="form-control" placeholder="Fullname" type="text" value="{{ $user->nama_pelanggan }}">
                @if ($errors->has('name'))
                    <span class="help-block alert-danger">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div> <!-- form-group// -->
            <div class="form-group"> 
                <label>ID Pelanggan</label>
                <input class="form-control" type="number" name="nomor_kwh" value="{{ $user->nomor_kwh }}">
            </div> <!-- form-group// -->  
            <div class="form-group">
                <label>Alamat</label>
                <textarea name="alamat" class="form-control">{{ $user->alamat }}</textarea>
            </div> <!-- form-group// --> 
            <div class="form-group">
                <label>Gol/Tarif</label>
                <select name="id_tarif" class="form-control">
                    <option value="{{ $user->id_tarif }}">{{ $user->tarif->daya }}</option>
                    <option value="1">R1/900</option>
                    <option value="2">R1/1300</option>
                    <option value="3">P1/16500</option>
                </select>
            </div> <!-- form-group// --> 
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block"> Update  </button>
            </div> <!-- form-group// -->                                                           
        </form>
    </article>
</div> <!-- card.// -->

{{-- <div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">

            <div class="panel-body">
                <a href="{{ route('admin.dashboard') }}" class="nav navbar-nav">Home</a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">Daftar Admin Baru</a></div>

            <div class="panel-body">
                <!-- <div class="container"> -->
                    <form class="form-horizontal" method="POST" action="{{ route('admin.register.submit') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-4 control-label">Username</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required>

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="id_level" class="col-md-4 control-label">Level</label>

                            <div class="col-md-6">
                                <select name="id_level" class="form-control">
                                    <option value="2">Administrator</option>
                                    <option value="3">Bank</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                <!-- </div> -->
            </div>
        </div>
    </div>
</div> --}}
@endsection
