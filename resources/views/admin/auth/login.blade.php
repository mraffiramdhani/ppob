@extends('admin.layouts.app')

@section('content')

<div class="card">
    <article class="card-body">
        <h4 class="card-title mb-4 mt-1">Admin Login</h4>
        <form class="form-horizontal" method="POST" action="{{ route('admin.login.submit') }}">
            {{ csrf_field() }}
            <div class="form-group">
                <label>Your username</label>
                <input name="username" class="form-control" placeholder="Username" type="username">
            </div> <!-- form-group// -->
            <div class="form-group">
                <label>Your password</label>
                <input class="form-control" placeholder="******" type="password" name="password">
            </div> <!-- form-group// --> 
            <div class="form-group"> 
            <div class="checkbox">
              <label> <input type="checkbox" name="remember"> Remember me </label>
            </div> <!-- checkbox .// -->
            </div> <!-- form-group// -->  
            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block"> Login  </button>
            </div> <!-- form-group// -->                                                           
        </form>
    </article>
</div> <!-- card.// -->

@endsection
