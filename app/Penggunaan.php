<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penggunaan extends Model
{
    protected $primaryKey = 'id_penggunaan';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_pelanggan', 'bulan', 'tahun', 'meter_awal', 'meter_akhir',
    ];

    public function user()
    {
    	return $this->belongsTo('App\User', 'id_pelanggan', 'id_pelanggan');
    }

    public function tagihan()
    {
        return $this->hasOne('App\Tagihan', 'id_penggunaan', 'id_penggunaan');
    }
}
