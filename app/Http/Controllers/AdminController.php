<?php

namespace App\Http\Controllers;

use PDF;
use App\User;
use App\Admin;
use App\Pembayaran;
use App\Penggunaan;
use App\Tagihan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $penggunaan = Penggunaan::orderBy('id_penggunaan', 'desc')->get();
        $pembayaran = Pembayaran::orderBy('id_pembayaran', 'desc')->get();
        return view('admin.pages.home', compact('users', 'penggunaan', 'pembayaran'));
    }
    public function jumlahMeter($param)
    {
        $jumlah = $param->meter_akhir - $param->meter_awal;
        return $jumlah;
    }
    public function totalBayar($param)
    {
        $tarif = $param->user->tarif->tarifperkwh;
        $total = $param->jumlah_meter * $tarif;
        return $total;
    }
    public function confirm($id)
    {
        $bayar = Pembayaran::where('id_pembayaran', $id)->first();
        $bayar->id_admin = \Auth::guard('admin')->user()->id_admin;
        $bayar->save();

        $latest = Tagihan::where('id_tagihan', $bayar->id_tagihan)->first();
        $penggunaan = new Penggunaan();
        $penggunaan->id_pelanggan = $latest->id_pelanggan;
        $penggunaan->bulan = date("Y-m-d", strtotime($latest->bulan . " +1 month"));
        $penggunaan->tahun = date("Y-m-d", strtotime($latest->bulan . " +1 month"));
        $penggunaan->meter_awal = $latest->penggunaan->meter_akhir;
        $penggunaan->meter_akhir = $latest->penggunaan->meter_akhir;
        $penggunaan->save();
        
        return redirect()->route('admin.dashboard')->with('msg', 'Data telah di konfirmasi');
    }
    public function tagihanConfirm($id)
    {
        $tagihan = Tagihan::where('id_tagihan', $id)->first();
        $tagihan->id_status = 3;
        $tagihan->save();

        return redirect()->route('admin.dashboard')->with('msg', 'Data telah di konfirmasi');
    }
    public function registerPage()
    {
        return view('admin.pages.register');
    }
    public function userEditPage($id)
    {
        $user = User::find($id)->first();
        return view('admin.pages.edit-user', compact('user'));
    }
    public function userEdit(Request $req, $id)
    {
        $user = User::find($id)->first();
        $user->nama_pelanggan = $req->name;
        $user->nomor_kwh = $req->nomor_kwh;
        $user->alamat = $req->alamat;
        $user->id_tarif = $req->id_tarif;
        $user->save();
        return redirect()->route('admin.dashboard')->with('msg', 'Data telah berhasil di ubah');
    }
    public function tagihanSend($id)
    {
        $penggunaan =  Penggunaan::where('id_penggunaan', $id)->first();
        $user = User::find($penggunaan->id_pelanggan)->first();
        $tagihan = new Tagihan();
        $tagihan->id_penggunaan = $penggunaan->id_penggunaan;
        $tagihan->id_pelanggan = $penggunaan->id_pelanggan;
        $tagihan->bulan = $penggunaan->bulan;
        $tagihan->tahun = $penggunaan->tahun;
        $tagihan->jumlah_meter = $this->jumlahMeter($penggunaan);
        $tagihan->id_status = 1;
        $tagihan->save();
        return redirect()->route('admin.dashboard')->with('msg', 'Tagihan telah berhasil di kirimkan');
    }
    public function regAdmin(Request $req)
    {
        $admin = new Admin();
        $admin->nama_admin = $req->name;
        $admin->username = $req->username;
        $admin->password = bcrypt($req->password);
        $admin->id_level = $req->id_level;
        $admin->save();
        return redirect()->route('admin.register.new')->with('msg', 'Admin Baru Telah Berhasil Di Buat.');
    }
    public function deleteUser($id)
    {
        Pembayaran::where('id_pelanggan', $id)->delete();
        Tagihan::where('id_pelanggan', $id)->delete();
        Penggunaan::where('id_pelanggan', $id)->delete();
        $user = User::where('id_pelanggan', $id)->delete();
        return redirect()->route('admin.dashboard')->with('msg', 'Data berhasil di hapus');
    }
    public function userListReport()
    {
        $users = User::all();
        $pdf = PDF::loadView('admin.report.user-list', compact('users'));

        return $pdf->stream('user-list.pdf');
    }
    public function userBillReport()
    {   
        $bill = Tagihan::all();
        $pdf = PDF::loadView('admin.report.bill-list', compact('bill'));

        return $pdf->stream('user-bill-list.pdf');
    }
}
