<?php

namespace App\Http\Controllers;

use PDF;
use App\Penggunaan;
use App\Tagihan;
use App\Pembayaran;
use App\Tarif;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:web');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $uid = \Auth::user()->id_pelanggan;
        $penggunaan = Penggunaan::where('id_pelanggan', $uid)->orderBy('id_penggunaan', 'DESC')->first();
        $tarif = \Auth::user()->tarif()->first();
        $tg_all = Tagihan::where('id_pelanggan', $uid)->orderBy('id_penggunaan', 'DESC')->get();
        return view('home', compact('penggunaan', 'tarif', 'tg_all'));
        // return dd($tg_all);
    }

    public function paymentPage(Request $req)
    {
        $tagihan = Tagihan::find($req->id)->orderBy('id_tagihan', 'DESC')->first();
        return view('pages.payment', compact('tagihan'));
        // return response()->json($tagihan);
    }

    public function payment(Request $req, $id)
    {
        $biaya_admin = 2000;
        $tagihan = Tagihan::where('id_tagihan', $id)->first();

        $bayar = new Pembayaran();
        $bayar->id_tagihan = $id;
        $bayar->id_pelanggan = \Auth::user()->id_pelanggan;
        $bayar->bulan_bayar = $tagihan->bulan;
        $bayar->akun_bank = $req->akun_bank;
        $bayar->total_bayar = ($tagihan->jumlah_meter * $tagihan->user->tarif->tarifperkwh) + $biaya_admin;
        $bayar->save();

        $tagihan->id_status = 2;
        $tagihan->save();
        
        return redirect()->route('home')->with('msg', 'Transaksi Berhasil');
    }

    public function printBill(Request $req, $id)
    {
        $tagihan = Tagihan::where('id_tagihan', $id)->first();
        $tanggal = date('F-Y', strtotime($tagihan->bulan));
        $pdf = PDF::loadView('report.bill-print', compact('tagihan'));
        $pdf->setPaper('half-letter', 'landscape');

        return $pdf->stream('Tagihan-' . $tanggal . '.pdf');
    }

}
