<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $primaryKey = 'id_pelanggan';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_pelanggan', 'username', 'password', 'nomor_kwh', 'alamat', 'id_tarif',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function tarif()
    {
        return $this->belongsTo('App\Tarif', 'id_tarif', 'id_tarif');
    }

    public function penggunaan()
    {
        return $this->hasMany('App\Penggunaan', 'id_pelanggan', 'id_pelanggan');
    }

    public function tagihan()
    {
        return $this->hasMany('App\Tagihan', 'id_pelanggan', 'id_pelanggan');
    }
}
