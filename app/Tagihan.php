<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tagihan extends Model
{
    protected $primaryKey = 'id_tagihan';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_penggunaan', 'id_pelanggan', 'bulan', 'tahun', 'jumlah_meter', 'id_status',
    ];

    public function status()
    {
    	return $this->belongsTo('App\Status', 'id_status', 'id_status');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'id_pelanggan', 'id_pelanggan');
    }

    public function penggunaan()
    {
        return $this->belongsTo('App\Penggunaan', 'id_penggunaan', 'id_penggunaan');
    }

    public function pembayaran()
    {
        return $this->hasOne('App\Pembayaran', 'id_tagihan', 'id_tagihan');
    }

}
