<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembayaran extends Model
{
    protected $primaryKey = 'id_pembayaran';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_tagihan', 'id_pelanggan', 'tanggal_pembayaran', 'bulan_bayar', 'biaya_admin', 'total_bayar', 'akun_bank', 'id_admin',
    ];

    public function user()
    {
    	return $this->belongsTo('App\User', 'id_pelanggan', 'id_pelanggan');
    }

    public function admin()
    {
        return $this->belongsTo('App\Admin', 'id_admin', 'id_admin');
    }
}
