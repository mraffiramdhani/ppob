<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('user')->group(function(){
	Route::post('logout', 'Auth\loginController@userLogout')->name('user.logout');
	Route::post('/payment', 'HomeController@paymentPage')->name('user.payment');
	Route::post('/payment/{id}', 'HomeController@payment')->name('user.payment.submit');

	// Laporan
	Route::get('/bill/report/{id}', 'HomeController@printBill')->name('user.bill.print');

});

Route::prefix('admin')->group(function(){
	Route::get('login', 'Auth\AdminLoginController@showFormLogin')->name('admin.login');
	Route::post('login', 'Auth\AdminLoginController@adminLogin')->name('admin.login.submit');
	Route::post('logout', 'Auth\AdminLoginController@adminLogout')->name('admin.logout.submit');
	Route::get('/', 'AdminController@index')->name('admin.dashboard');
	Route::get('/payment/confirm/{id}', 'AdminController@confirm')->name('admin.payment.confirm');
	Route::get('register', 'AdminController@registerPage')->name('admin.register.new');
	Route::post('register', 'AdminController@regAdmin')->name('admin.register.submit');
	Route::post('/u/delete/{id}', 'AdminController@deleteUser')->name('admin.user.delete');
	Route::get('/u/edit/{id}', 'AdminController@userEditPage')->name('admin.user.edit');
	Route::patch('/u/edit/{id}', 'AdminController@userEdit')->name('admin.user.edit.submit');
	Route::get('/bill/{id}', 'AdminController@tagihanSend')->name('admin.bill.send');
	Route::get('/bill/confirm/{id}', 'AdminController@tagihanConfirm')->name('admin.bill.confirm');

	Route::get('/report/user/list', 'AdminController@userListReport')->name('admin.report.user-list');
	Route::get('/report/bill/list', 'AdminController@userBillReport')->name('admin.report.bill-list');	
});
